from unittest import TestCase
from unittest.mock import patch
from services import tournaments_service
from data.models import Tournament, MatchUp, Player, LeagueStanding
from datetime import date
from common.responses import Unauthorized



class TournamentServiceShould(TestCase):
    
    @patch("services.tournaments_service.insert_query")
    def test_createTournament_returnsCorrectly(self, mock_query):
        mock_query.return_value = 1
        tournament = Tournament(title="Test Tournament", prize="1000 lv", format_id=1)

        result = tournaments_service.create_tournament(tournament)
        expected = Tournament(id=1, title="Test Tournament", prize="1000 lv", format_id=1)

        return self.assertEqual(result, expected)
    
    
    @patch("services.tournaments_service.create_tournament")
    @patch("services.tournaments_service.create_random_matchups")
    @patch("services.tournaments_service.create_empty_phase")
    @patch("services.tournaments_service.player_service.create_unknown_participants_profile")
    def test_createKnockOutTournament_returnsCorrectly(self, mock_participants, mock_empty_phase, mock_random_matchups, mock_tournament):
        mock_participants.return_value = [["Vladimir", "Putin"], ["Joe", "Biden"], ["Recep", "Erdogan"], ["Emanuel", "Macron"]]
        mock_empty_phase.return_value = None
        mock_random_matchups.return_value = None
        mock_tournament.return_value = Tournament(id=1, title="Test Tournament", prize="1000 lv", format_id=1)

        tournament = Tournament(title="Test Tournament", prize="1000 lv", format_id=1)
        participants = ["Vladimir Putin", "Joe Biden", "Recep Erdogan", "Emanuel Macron"]
        starting_date = date(2022, 12, 23)

        result = tournaments_service.create_knockout_tournament(tournament, participants, starting_date)
        expected = None

        self.assertEqual(result, expected)
    


    def test_createKnockOutTournament_returnsUnauthorized(self):
        tournament = Tournament(title="Test Tournament", prize="1000 lv", format_id=1)
        participants = ["Vladimir Putin", "Joe Biden", "Recep Erdogan"]
        starting_date = date(2022, 12, 23)

        result = tournaments_service.create_knockout_tournament(tournament, participants, starting_date)
        expected = Unauthorized

        self.assertIsInstance(result, expected)
    
    
    @patch("services.tournaments_service.read_query")
    def test_getMatchupIdsCurrentPhase_returnsCorrectly(self, mock_query):
        mock_query.return_value = [[1],[2],[3],[4],[5],[6],[7],[8]]
        matchup = MatchUp(id=1, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        result = tournaments_service.get_matchup_ids_current_phase(matchup)
        expected = [[1,2],[3,4],[5,6],[7,8]]

        self.assertEqual(result, expected)


    @patch("services.tournaments_service.read_query")
    def test_getMatchUpIdsNextPhase_returnsCorrectly(self, mock_query):
        mock_query.return_value = [[9],[10],[11],[12]]
        matchup = MatchUp(id=1, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        result = tournaments_service.get_matchup_ids_next_phase(matchup)
        expected = [9,10,11,12]

        self.assertEqual(result, expected)
    

    @patch("services.tournaments_service.read_query")
    def test_getMatchUpIdsNextPhase_returnsCorrectlyWhenFinal(self, mock_query):
        mock_query.return_value = []
        matchup = MatchUp(id=13, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        result = tournaments_service.get_matchup_ids_next_phase(matchup)
        expected = []

        self.assertEqual(result, expected)
    

    def test_getRightId_returnsCorrectly(self):
        current_ids = [[1,2],[3,4],[5,6],[7,8]]
        next_ids = [9,10,11,12]
        matchup = MatchUp(id=5, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        result = tournaments_service.get_right_id(matchup,current_ids, next_ids)
        expected = 11

        self.assertEqual(result, expected)
    

    def test_getRightId_returnsCorrectlyWhenFinal(self):
        current_ids = [[15]]
        next_ids = []
        matchup = MatchUp(id=15, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        result = tournaments_service.get_right_id(matchup,current_ids, next_ids)
        expected = None

        self.assertEqual(result, expected)
    
    
    @patch("services.tournaments_service.read_query")
    def test_getMatchup_returnsCorrectly(self, mock_query):
        mock_query.return_value = [[1,1,date(2022, 12, 23),1,1,2,10,15]]

        result = tournaments_service.get_matchup(1)
        expected = MatchUp(id=1,tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=1, player_one=1, player_two=2, player_one_score=10, player_two_score=15)

        self.assertEqual(result, expected)


    @patch("services.tournaments_service.read_query")
    def test_getMatchup_returnsCorrectlyWhenFinal(self, mock_query):
        mock_query.return_value = []

        result = tournaments_service.get_matchup(None)
        expected = None

        self.assertEqual(result, expected)


    @patch("services.tournaments_service.get_right_id")
    @patch("services.tournaments_service.get_matchup_ids_next_phase")
    @patch("services.tournaments_service.get_matchup_ids_current_phase")
    @patch("services.tournaments_service.get_matchup")
    def test_getNextMatchup_returnsCorrectly(self, mock_matchup, mock_current_phase, mock_next_phase, mock_right_id):
        mock_current_phase.return_value = [[9,10],[11,12]]
        mock_next_phase.return_value = [13, 14]
        mock_right_id.return_value = 14
        mock_matchup.return_value = MatchUp(id=14, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2)

        matchup = MatchUp(id=11, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2)

        result = tournaments_service.get_next_matchup(matchup)
        expected = MatchUp(id=14, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2)

        self.assertEqual(result, expected)

    
    @patch("services.tournaments_service.get_right_id")
    @patch("services.tournaments_service.get_matchup_ids_next_phase")
    @patch("services.tournaments_service.get_matchup_ids_current_phase")
    @patch("services.tournaments_service.get_matchup")
    def test_getNextMatchup_returnsCorrectlyWhenFinal(self, mock_matchup, mock_current_phase, mock_next_phase, mock_right_id):
        mock_current_phase.return_value = [[15]]
        mock_next_phase.return_value = []
        mock_right_id.return_value = None
        mock_matchup.return_value = None

        matchup = MatchUp(id=15, tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2)

        result = tournaments_service.get_next_matchup(matchup)
        expected = None

        self.assertEqual(result, expected)


    @patch("services.tournaments_service.insert_query")
    def test_createLeague_returnsCorrectly(self, mock_query):
        mock_query.return_value = 1

        league = Tournament(title="Test League", prize="1000 lv", format_id=2)

        result = tournaments_service.create_tournament(league)
        expected = Tournament(id= 1, title="Test League", prize="1000 lv", format_id=2)

        self.assertEqual(result, expected)


    @patch("services.tournaments_service.read_query")
    def test_getLeagueMatchup_returnsCorrectly(self, mock_query):
        mock_query.return_value = [[1,1,date(2022, 12, 23),2,1,2,2,3]]

        result = MatchUp(id=1,tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2, player_one=1, player_two=2, player_one_score=3, player_two_score=2)
        expected = MatchUp(id=1,tournament_id=1, played_at=date(2022, 12, 23), tournament_phase=2, player_one=1, player_two=2, player_one_score=3, player_two_score=2)

        self.assertEqual(result, expected)


    def test_createPhase_returnsUnauthorized(self):
        participants = ["Vladimir Putin", "Joe Biden", "Recep Erdogan"]
        
        result = len(participants)-1
        expected = 2

        self.assertEqual(result, expected)


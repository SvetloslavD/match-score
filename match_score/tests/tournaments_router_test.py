
from unittest import mock
from unittest.mock import patch
import unittest
from data.models import Tournament, All_Tournaments, TournamentResponseModel, User, Scores, MatchUp, LeagueStanding,Participants
from services import user_service
from common.responses import NotFound, Unauthorized, Successful, NoContent, BadRequest
from services.tournaments_service import sort
from routers import tournaments
from datetime import date, datetime


class Tournaments_Should(unittest.TestCase):

    @patch('routers.tournaments.tournaments_service')
    def test_allTournaments_returnsCorrectly(self, mock_tournaments):
        mock_tournaments.all.return_value = [
        All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] ),
        All_Tournaments(id=2, title='TestTournament2', prize='2000 lv', format_id=2, winner=None, players=[4,5,6,7], matchups=[3,4])
        ]

        result = tournaments.get_tournaments(None, None)
        expected = [
        All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] ),
        All_Tournaments(id=2, title='TestTournament2', prize='2000 lv', format_id=2, winner=None, players=[4,5,6,7], matchups=[3,4])
        ]

        self.assertEqual(result, expected)

    @patch('routers.tournaments.tournaments_service')
    def test_allTournaments_returnsCorrectlyWhenSearch(self, mock_tournaments):
        mock_tournaments.all.return_value = All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] )

        result = tournaments.get_tournaments(None, 'Test')
        expected = All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] )
        
        self.assertEqual(result, expected)
    
    @patch('routers.tournaments.tournaments_service')
    def test_allTournaments_returnsCorrectlyWhenNoTournaments(self, mock_tournaments):
        mock_tournaments.all.return_value = []

        result = tournaments.get_tournaments(None, None)
        expected_type = NotFound
        
        self.assertIsInstance(result, expected_type)
    
    @patch('routers.tournaments.tournaments_service')
    def test_allTournaments_returnsCorrectlyWhenSortDesc(self, mock_tournaments):
        mock_tournaments.all.return_value = [
        All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] ),
        All_Tournaments(id=2, title='TestTournament2', prize='2000 lv', format_id=2, winner=None, players=[4,5,6,7], matchups=[3,4])
        ]
        mock_tournaments.sort = sort
        result = tournaments.get_tournaments('desc', None)
        expected = [
            All_Tournaments(id=2, title='TestTournament2', prize='2000 lv', format_id=2, winner=None, players=[4,5,6,7], matchups=[3,4]),
        All_Tournaments(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[1,2,3,4], matchups=[1,2] )
        ]

        self.assertEqual(result, expected)


    @patch('routers.tournaments.tournaments_service')
    def test_Tournament_returnsCorrectly(self, mock_tournaments):
        mock_tournaments.get_by_id.return_value = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='Knockout', winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ])
        
        result = tournaments.get_tournament_by_id(1)
        expected = TournamentResponseModel(id=1, title='TestTournament1', prize='1000 lv', format='Knockout', winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ])
        
        self.assertEqual(result, expected)

    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_CreateTournament_returnsCorrectlyWhenAdmin(self,mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        mock_tournaments.send_email_to_linked_users.return_value = None
        mock_tournaments.create_knockout_tournament.return_value = None
    
        result = tournaments.create_knockout_tournament(Tournament(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[], matchups=[]), ['Steven Seagull','John Wick','Peter Griffin','Horatio Kayn'], date.today(), 'fake_token')
        expected_type = Successful

        self.assertIsInstance(result, expected_type)


    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_CreateTournament_returnsCorrectlyWhenAdminAndNoParticipants(self,mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        mock_tournaments.send_email_to_linked_users.return_value = None
        mock_tournaments.create_knockout_tournament.return_value = None
    
        result = tournaments.create_knockout_tournament(Tournament(id=1, title='TestTournament1', prize='1000 lv', format_id=1, winner=None, players=[], matchups=[]), [], date.today(), 'fake_token')
        expected_type = Unauthorized

        self.assertIsInstance(result, expected_type)
    

    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_UpdateTournament_returnsDataCorrectlyWhenAdmin(self,mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        
        mock_tournaments.get_by_id.return_value = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='Knockout', winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ])
        
        mock_tournaments.check_for_existing_format.return_value = True
        mock_tournaments.check_for_existing_player.return_value = True
        
        mock_tournaments.update.return_value = Tournament(
        id=1, title='TestTournament1', prize='3333 lv', format_id=1, winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ])
        
        result = tournaments.update_tournament(
        1,
        Tournament(id=1, title='TestTournament1', prize='3333 lv', format_id=1, winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ]), 'fake_token')

        expected = Tournament(id=1, title='TestTournament1', prize='3333 lv', format_id=1, winner=None,
        players=[
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None},
        {'id':None, 'first_name':None,'second_name':None, 'country':None, 'team':None}
        ],
        matchups=[
        {'id':None, 'tournament_id':None, 'played_at':None, 'tournament_phase':None, 'player_one':None, 'player_two':None, 'player_one_score':None, 'player_two_score':None}
        ])

        self.assertEqual(result, expected)
    
    
    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_CreateLeague_returnsCorrectlyWhenAdmin(self,mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        
        mock_tournaments.check_for_existing_format.return_value = True
        mock_tournaments.send_email_to_linked_users.return_value = None
        mock_tournaments.create_knockout_tournament.return_value = None
    
        result = tournaments.create_league(Tournament(id=1, title='TestTournament1', prize='1000 lv', format_id=2, winner=None, players=[], matchups=[]), ['Steven Seagull','John Wick','Peter Griffin','Horatio Kayn'], date.today(), 'fake_token')
        expected_type = Successful

        self.assertIsInstance(result, expected_type)
    
    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_SetLeagueScore_returnsCorrectlyWhenAlreadyHasScore(self, mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        mock_tournaments.get_matchup.return_value = MatchUp(id=1, tournament_id=1, played_at=date.today(), tournament_phase= 1, player_one= 1, player_two=2, player_one_score=1, player_two_score=2)
       
        result = tournaments.set_league_score(1, Scores(score_one = 1, score_two = 4), 'fake_token')
        expected_type = BadRequest

        self.assertIsInstance(result, expected_type)
    
    @patch('routers.tournaments.tournaments_service')
    def test_GetLeagueStandings_returnsCorrectly(self, mock_tournaments):
        mock_tournaments.get_by_id.return_value = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='league', winner=None,
        players=[
        {'id':1, 'first_name':'Steven','second_name':'Seagull', 'country':'USA', 'team':None},
        {'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}
        ],
        matchups=[
        {'id':1, 'tournament_id':1, 'played_at':date.today(), 'tournament_phase':1, 'player_one':1, 'player_two':2, 'player_one_score':1, 'player_two_score':4}
        ])
        
        mock_tournaments.get_standings.return_value = [
        LeagueStanding(player ={'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}, points = 3),
        LeagueStanding(player = {'id':1, 'first_name':'Steven','second_name':'Seagull', 'country':'USA', 'team':None}, points = 1)
        ]
        
        result = tournaments.view_league_standings(1, 'asc')
        expected = [
        LeagueStanding(player = {'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}, points = 3),
        LeagueStanding(player = {'id':1, 'first_name':'Steven','second_name':'Seagull', 'country':'USA', 'team':None}, points = 1)
        ]

        self.assertEqual(result, expected)
    
    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_ForceUpdateMatchup_returnsCorrectly(self, mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        mock_tournaments.get_matchup.return_value = MatchUp(id=1, tournament_id=1, played_at=date.today(), tournament_phase= 1, player_one= 1, player_two=2, player_one_score=1, player_two_score=2)
        mock_tournaments.force_update_matchup.return_value = None
        mock_tournaments.get_matchup.return_value = MatchUp(id=1, tournament_id=1, played_at=date.today(), tournament_phase= 1, player_one= 1, player_two=2, player_one_score=7, player_two_score=6)
        
        
        result = tournaments.force_update_matchup(1, MatchUp(id=1, tournament_id=1, played_at=date.today(), tournament_phase= 1, player_one= 1, player_two=2, player_one_score=7, player_two_score=6), 'fake_token')
        expected = MatchUp(id=1, tournament_id=1, played_at=date.today(), tournament_phase= 1, player_one= 1, player_two=2, player_one_score=7, player_two_score=6)

        self.assertEqual(result, expected)
    
    @patch('routers.tournaments.tournaments_service')
    @patch('routers.tournaments.user_service')
    def test_ForceUpdateParticipants_returnsCorrectly(self, mock_users, mock_tournaments):
        mock_users.from_token.return_value = 'fake_credentials'
        mock_users.find_user.return_value = User(id=1, email='john@gmail.com', password='john1', role='admin', associated_player = None)
        mock_tournaments.get_by_id.return_value = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='league', winner=None,
        players=[
        {'id':1, 'first_name':'Steven','second_name':'Seagull', 'country':'USA', 'team':None},
        {'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}
        ],
        matchups=[
        {'id':1, 'tournament_id':1, 'played_at':date.today(), 'tournament_phase':1, 'player_one':1, 'player_two':2, 'player_one_score':1, 'player_two_score':4}
        ])
        mock_tournaments.get_starting_date.return_value = date.today()
        mock_tournaments.delete_matchups.return_value = None
        mock_tournaments.create_new_matchups.return_value = None
        mock_tournaments.get_by_id.return_value = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='league', winner=None,
        players=[
        {'id':1, 'first_name':'Jack','second_name':'Sparow', 'country':'USA', 'team':None},
        {'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}
        ],
        matchups=[
        {'id':1, 'tournament_id':1, 'played_at':date.today(), 'tournament_phase':1, 'player_one':1, 'player_two':2, 'player_one_score':1, 'player_two_score':4}
        ])

        result = tournaments.force_update_participants(1, Participants(participants=['Jack Sparow','Alice Ding']), 'fake_token')
        expected = TournamentResponseModel(
        id=1, title='TestTournament1', prize='1000 lv', format='league', winner=None,
        players=[
        {'id':1, 'first_name':'Jack','second_name':'Sparow', 'country':'USA', 'team':None},
        {'id':2, 'first_name':'Alice','second_name':'Ding', 'country':'China', 'team':None}
        ],
        matchups=[
        {'id':1, 'tournament_id':1, 'played_at':date.today(), 'tournament_phase':1, 'player_one':1, 'player_two':2, 'player_one_score':1, 'player_two_score':4}
        ])

        self.assertEqual(result, expected)
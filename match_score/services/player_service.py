from data import database
from data.models import Player, PlayerStatistics, User
from services import team_service
from common.responses import Unauthorized, BadRequest


def country_names():
    country_names = database.read_query("SELECT name from country")

    return [i[0] for i in country_names]


def country_id(name: str):
    id = database.read_query("SELECT id from country where name = ?", (name,))

    return id[0][0]


def create_player(player: Player):
    if player.country not in country_names(): return None
    if player.team and player.team not in team_service.team_names(): return None

    if player.team: team_id = team_service.team_id(player.team)
    else: team_id = None
    
    generated_id = database.insert_query("""INSERT INTO player (first_name, second_name, team_id, country_id) 
    values (?,?,?,?)""", (player.first_name, player.second_name, team_id, country_id(player.country)))

    player.id = generated_id

    return player


def all_players(country: str = None, team: str = None):
    if not country and not team:
        players = database.read_query("""SELECT player.id, first_name, second_name, country.name, team.name 
        from player LEFT JOIN team on team_id = team.id
        LEFT JOIN country on country_id = country.id""")
    elif country and not team:
        players = database.read_query("""SELECT player.id, first_name, second_name, country.name, team.name 
        from player JOIN country on country_id = country.id
        LEFT JOIN team on team_id = team.id
        where country.name = ?""", (country,))
    elif team and not country: 
        players = database.read_query("""SELECT player.id, first_name, second_name, country.name, team.name 
        from player JOIN team on team_id = team.id
        LEFT JOIN country on country_id = country.id
        where team.name = ?""", (team,))
    elif team and country: 
        players = database.read_query("""SELECT player.id, first_name, second_name, country.name, team.name 
        from player JOIN team on team_id = team.id
        JOIN country on country_id = country.id
        where team.name = ? and country.name = ?""", (team, country))

    return [Player.from_query_result(p[0], p[1], p[2], p[3], p[4]) for p in players]


def get_player_by_id(id: int):
    player = database.read_query("""SELECT player.id, first_name, second_name, country.name, team.name 
        from player LEFT JOIN team on team_id = team.id
        LEFT JOIN country on country_id = country.id where player.id = ?""", (id,))
    
    if not player: return None
    player = player[0]
    return Player.from_query_result(player[0], player[1], player[2], player[3], player[4])


def update_player_team(id: int, player: Player):
    if player.team:
        if player.team not in team_service.team_names(): 
            return None
        else:  
            t_id = team_service.team_id(player.team)
            database.update_query("UPDATE player SET team_id = ? where id = ?", (t_id, id))
    else:
        database.update_query("UPDATE player SET team_id = NULL where id = ?", (id,))
    
    return True


def null_team(team_id: int):
    database.update_query("UPDATE player set team_id = ? where team_id = ?", (None, team_id))


def delete_player(id: int):
    database.update_query("DELETE from player where id = ?", (id,))


def get_player_by_name_from_class(player: Player):
    data = database.read_query("SELECT id, first_name, second_name, team_id, country_id from player where first_name = ? and second_name = ?", 
    (player.first_name, player.second_name))
    if not data: return None
    p = data[0]

    return Player.from_query_result(p[0], p[1], p[2], p[3], p[4])


def is_linked_to_user(player: Player):
    data = database.read_query("SELECT * from user where associated_player = ?", (player.id,))
    if not data: return None
    user = data[0]
    return User.from_query_result(user[0], user[1], user[2], user[3], user[4])


def get_tournament_players(id: int):       
    player_data = database.read_query("""SELECT player_one, p.first_name, p.second_name, c.name, t.name, player_two, pt.first_name, pt.second_name, ct.name, tt.name
    from matchups LEFT JOIN player as p on matchups.player_one = p.id LEFT JOIN team as t on p.team_id = t.id LEFT JOIN country as c on p.country_id = c.id
    LEFT JOIN player as pt on matchups.player_two = pt.id LEFT JOIN team as tt on pt.team_id = tt.id LEFT JOIN country as ct on pt.country_id = ct.id
    where tournament_id = ? and player_one is not NULL and player_two is not NULL""", (id,))
    
    players = []
    for d in player_data: players.extend([Player.from_query_result(d[0], d[1], d[2], d[3], d[4]), Player.from_query_result(d[5], d[6], d[7], d[8], d[9])])
    
    unique_players = []
    for p in players:
        if p not in unique_players:
            unique_players.append(p)
    
    return unique_players


def get_player_by_name(fullname: list[str]):
    data = database.read_query(
                '''SELECT * FROM player
                    WHERE first_name = ? and second_name = ?''', (fullname[0],fullname[1]))                  
    if not data: return None
    player = data[0]
    return Player.from_query_result(player[0], player[1], player[2], player[3], player[4])


def create_player_by_name(fullname: str): 
    data = database.insert_query("""INSERT INTO player (first_name, second_name, team_id, country_id) 
    values (?,?,?,?)""", (fullname[0], fullname[1], None, None))


def create_unknown_participants_profile(participants: list[str]):
    players = all_players()
    current_names = []
    existing_names = []
    for e in players:
        existing_names.append([e.first_name, e.second_name])   
    for participant in participants:
        participant_first_name, participant_second_name = participant.split(" ")
        current_names.append([participant_first_name, participant_second_name]) 
    for p in current_names:
        if p not in existing_names:
            create_player_by_name(p)  
    return current_names

def get_statistics(id: int):
    player = get_player_by_id(id)   
    
    tournaments_played = database.read_query(
    """SELECT DISTINCT tournament.title as Tournament from matchups
    LEFT JOIN player as p on matchups.player_one = p.id 
    LEFT JOIN tournament on tournament_id = tournament.id where p.id = ?
    UNION
    SELECT DISTINCT tournament.title as Tournament from matchups
    LEFT JOIN player as p on matchups.player_two = p.id 
    LEFT JOIN tournament on tournament_id = tournament.id where p.id = ?""", (id, id))
    
    tournaments_won = database.read_query(
        '''SELECT t.title FROM tournament as t
        LEFT JOIN player on player.id = t.winner WHERE player.id = ?''', (id,))
    
    matches_played = database.read_query(
        '''SELECT m.id, m.title, m.played_at, m_d.score as player_score FROM match_score.match as m
        LEFT JOIN player_match_detail as m_d on m_d.match_id = m.id
        LEFT JOIN player on player.id = m_d.player_id
        WHERE player.id = ?''', (id,))
        # Changed table name to player_match_detail from match_detail

    most_often_opponent = database.read_query(
        '''SELECT most_often.opponent FROM 
        (SELECT Count(u.player_one) as 'games_played', u.player_one as opponent FROM 
        (SELECT player_one FROM match_score.matchups WHERE player_two = ? 
        UNION ALL 
        SELECT player_two FROM match_score.matchups WHERE player_one = ?) as u
        GROUP BY u.player_one) as most_often
        ORDER BY most_often.games_played DESC, most_often.opponent LIMIT 1''', (id, id,))

    best_opponent = database.read_query(
        '''SELECT AVG(m.player_one_score>player_two_score)*100 as home_win_rate
            FROM match_score.matchups m
            WHERE m.player_one = ?
            GROUP BY m.player_one
            UNION
            SELECT avg(m.player_one_score<player_two_score)*100 as away_win_rate
            FROM match_score.matchups m
            WHERE m.player_two = ?
            GROUP BY m.player_two''', (id, id,))
  

    worst_opponent = database.read_query(
        '''SELECT AVG(m.player_one_score<player_two_score)*100 as home_win_rate
            FROM match_score.matchups m
            WHERE m.player_one = ?
            GROUP BY m.player_one
            UNION
            SELECT avg(m.player_one_score>player_two_score)*100 as away_win_rate
            FROM match_score.matchups m
            WHERE m.player_two = ?
            GROUP BY m.player_two''', (id, id,))


    winners_ids = []
    matches_won = []
    for m in matches_played:
        m=m[0]
        winners = database.read_query(
        '''select * from player_match_detail where match_id = ? order by score desc limit 1''', (m,))        # Changed table name to player_match_detail from match_detail
        #winners = winners[0]
        winners_ids.append(winners[0])
    
    for winner in winners_ids:
        if winner[0] == player.id:
            matches_won.append(winner[1])

    return PlayerStatistics(player=player, tournaments_played=[t[0] for t in tournaments_played], tournaments_won=[t[0] for t in tournaments_won],
     matches_played=[m[1] for m in matches_played],matches_won=matches_won, most_often_played_oponent=[moo[0] for moo in most_often_opponent], 
     win_percentage=[b[0] for b in best_opponent], lose_percentage=[w[0] for w in worst_opponent])



from datetime import date, timedelta
import random
from common.responses import BadRequest, Unauthorized
from data.models import All_Tournaments, LeagueStanding, LinkedUserDetails, MatchUp, Tournament, Player, TournamentResponseModel
from data.database import insert_query, read_query, update_query
from services import player_service, user_service
from mailjet_rest import Client



def all(search: str = None):  
    if search is None:
        data = read_query(
                '''SELECT  t.id, title, t.prize, t.tournament_format_id, t.winner, m.player_one, m.player_two, m.id as match_id
                    FROM tournament as t
                    LEFT JOIN matchups as m
                    ON m.tournament_id = t.id''')
    else:
        data = read_query(
                '''SELECT  t.id, title, t.prize, t.tournament_format_id, t.winner, m.player_one, m.player_two, m.id as match_id
                    FROM tournament as t
                    LEFT JOIN matchups as m
                    ON m.tournament_id = t.id
                    WHERE title LIKE ?''', (f'%{search}%',))
    
    flattened = {}
    for id, title, prize, format, winner, player_one, player_two, match_id in data:

        if id not in flattened:
            flattened[id] = (
            id, title, prize, format, winner, [], [])
        if player_one is not None and player_one not in flattened[id][-2]:
            flattened[id][-2].append(player_one)
        if player_two is not None and player_two not in flattened[id][-2]:
            flattened[id][-2].append(player_two)
        if match_id is not None:
            flattened[id][-1].append(match_id)

    return (All_Tournaments.from_query_result(*obj) for obj in flattened.values())


def sort(lst: list[Tournament], reverse=False):
    return sorted(
        lst,
        key=lambda t: t.id,
        reverse=reverse)


#========== Get matchup ============
def get_matchup(id: int):
    data = read_query("SELECT id, tournament_id, played_at, tournament_phase, player_one, player_two, player_one_score, player_two_score from matchups where id = ?", (id,))
    if not data: return None
    m = data[0]   
    return MatchUp(id=m[0],tournament_id=m[1], played_at=m[2], tournament_phase=m[3], player_one=m[4], player_two=m[5], player_one_score=m[6], player_two_score=m[7])
#==================================


#=============== Get tournamnet by id ==================
def get_tournament_matchups(id: int):
    data = read_query("SELECT * from matchups where tournament_id = ? order by tournament_phase", (id,))
    if not data: return None
    data = data[0]   
    return [MatchUp(id=m[0],tournament_id=m[1], played_at=m[2], tournament_phase=m[3], player_one=m[4], player_two=m[5], player_one_score=m[6], player_two_score=m[7]) for m in data]


def get_by_id(id: int):
    tournament_data = read_query(
            '''SELECT  t.id, title, t.prize, t.tournament_format_id, t.winner
                    FROM tournament as t WHERE t.id = ?''', (id,))
    
    players = player_service.get_tournament_players(id)
    matchups = get_tournament_matchups(id) or []
    tournament_format = get_tournament_format(id)
    
    if not tournament_data: return None
    tournament_data = tournament_data[0]
     
    return TournamentResponseModel(id=tournament_data[0], title=tournament_data[1], prize=tournament_data[2],format=tournament_format,
    winner=tournament_data[4],players=players,matchups=matchups)
#===============================


def get_tournament_format(id: int):
    data = read_query(
                '''SELECT  t_f.name
                    FROM tournament_format as t_f
                    LEFT JOIN tournament as t
                    ON t_f.id = t.tournament_format_id
                    WHERE t.id = ?''', (id,))
    if not data: return None
    tournament_format = data[0]                  
    return tournament_format[0]


def check_for_existing_player(id: int):
    return any(read_query('select * from player where id = ?',(id,)))


def check_for_existing_format(id: int):
    return any(read_query('select * from tournament_format where id = ?',(id,)))



#=========== Knockout tournament creation==============
def create_tournament(tournament: Tournament):
    generated_id = insert_query("INSERT INTO tournament (title, prize, tournament_format_id) values (?,?,?)",(tournament.title, tournament.prize, tournament.format_id))
    tournament.id = generated_id
    return tournament  


def create_random_matchups(tournament: Tournament, home: str, away: str, starting_date: date):
    player1 = player_service.get_player_by_name(home)
    player2 = player_service.get_player_by_name(away)
    insert_query('''insert into matchups values (?, ?, ?, ?, ?, ?, ?, ?)''',
    (None, tournament.id, starting_date, 1, player1.id, player2.id, None, None))


def create_empty_matchup(tournament: Tournament, date: date, phase: int):
    insert_query('''insert into matchups values (?, ?, ?, ?, ?, ?, ?, ?)''',
        (None, tournament.id, date, phase, None, None, None, None))


def create_empty_phase(tournament: Tournament, date: date, phase: int, p_count: int):
    matchup_count = p_count//2
    for _ in range(matchup_count): create_empty_matchup(tournament, date, phase)


def create_knockout_tournament(tournament: Tournament, participants: list[str], starting_date: date):
    if len(participants) in [4,8,16,32,64,128,256]:
        create_tournament(tournament)
        participants = player_service.create_unknown_participants_profile(participants)       
        phase = 2
        p_count = len(participants)//2     
        while len(participants)>1:
            create_random_matchups(tournament, participants[0], participants[1], starting_date)
            participants.pop(0)
            participants.pop(0)       
        while p_count > 1:
            create_empty_phase(tournament, starting_date, phase, p_count)
            phase += 1
            p_count //= 2
    else:
        return Unauthorized('Participants should be 4, 8, 16, 32, 64, 128 or 256 count!')  
#=======================================


#========== Moving winner to next phase for bracket ===============
def set_matchup_score(id: int, scores: list[int]):
    update_query('UPDATE matchups SET player_one_score = ?, player_two_score = ? WHERE id = ?', (scores[0], scores[1], id))


def update_participants_for_next_phase(next_matchup: MatchUp, matchup: MatchUp):
    winner_id = 0
    if matchup.player_one_score > matchup.player_two_score:
        winner_id = matchup.player_one
    elif matchup.player_two_score > matchup.player_one_score:
        winner_id = matchup.player_two
    
    if not next_matchup.player_one:
        data = update_query('''Update matchups SET player_one = ?
                    WHERE tournament_phase=? and id=?''', (winner_id, next_matchup.tournament_phase, next_matchup.id))       
    elif not next_matchup.player_two:
        data = update_query('''Update matchups SET player_two = ?
                    WHERE tournament_phase=? and id=?''', (winner_id, next_matchup.tournament_phase, next_matchup.id))


def get_matchup_ids_current_phase(matchup: MatchUp):
    data = read_query("SELECT id from matchups where tournament_id = ? and tournament_phase = ?", (matchup.tournament_id, matchup.tournament_phase))
    
    raw_ids = [i[0] for i in data]
    paired_ids = []
    
    count = 0
    current_pair = []
    for i in range(len(raw_ids)):
        if count > 0 and count % 2 == 0:
            paired_ids.append(current_pair.copy())
            current_pair.clear()
        
        current_pair.append(raw_ids[i])
        
        if i == len(raw_ids) - 1:
            paired_ids.append(current_pair)
        
        count += 1
    
    return paired_ids


def get_matchup_ids_next_phase(matchup: MatchUp):
    data = read_query("SELECT id from matchups where tournament_id = ? and tournament_phase = ?", (matchup.tournament_id, matchup.tournament_phase + 1))
    
    return [i[0] for i in data]


def get_right_id(matchup: MatchUp, current_ids: list[list[int]], next_ids: list[int]):
    if len(next_ids)>0:
        target_id = next((i for i in range(len(current_ids)) if matchup.id in current_ids[i]), None)
        
        return next_ids[target_id]


def get_next_matchup(matchup: MatchUp):
    current_ids = get_matchup_ids_current_phase(matchup)
    if len(current_ids)>0:
        next_ids = get_matchup_ids_next_phase(matchup)
        next_matchup_id = get_right_id(matchup, current_ids, next_ids)
        
        return get_matchup(next_matchup_id)
#=================================================


#========== LEAGUE CREATION ==============

def create_phase(league: Tournament, participants: list[str], phase, starting_date: date):
    phase %= (len(participants)-1)
    if phase:
        participants = participants[:1] + participants[-phase:] + participants[1:-phase]
    half = len(participants)//2
    phase_matchups = list(zip(participants[:half], participants[half:][::-1]))
    
    for m in phase_matchups:
        player_one = player_service.get_player_by_name(m[0])
        player_two = player_service.get_player_by_name(m[1])

        insert_query('''insert into matchups values (?, ?, ?, ?, ?, ?, ?, ?)''',
                (None, league.id, starting_date+timedelta(days=7*phase), phase+1, player_one.id, player_two.id, None, None))


def get_phases(participants: list[str]):
    phase = 1
    phases = [phase+1*i for i in range(len(participants)-1)]
    return phases


def create_league(league: Tournament, participants, starting_date: date):
    participants = player_service.create_unknown_participants_profile(participants)
    create_tournament(league)
    days = get_phases(participants)


    for day in days:
        create_phase(league, participants, day, starting_date)


#================ League Standing =================
def get_tournament_matchups(tournament_id: int):
    data = read_query("SELECT * FROM match_score.matchups where tournament_id = ?", (tournament_id,))
    if not data: return None

    return [MatchUp(id=m[0],tournament_id=m[1], played_at=m[2], tournament_phase=m[3], 
    player_one=m[4], player_two=m[5], player_one_score=m[6], player_two_score=m[7]) for m in data]


def get_league_standing(players: list[Player], matchups: list[MatchUp]):
    player_standing = [LeagueStanding(player=p, points=0) for p in players]

    for m in matchups:
    
        if m.player_one_score is None:
            continue
        
        elif m.player_one_score > m.player_two_score:
            for ps in player_standing:
                if ps.player.id == m.player_one:
                    ps.points += 3
        
        elif m.player_one_score < m.player_two_score:
            for ps in player_standing:
                if ps.player.id == m.player_two:
                    ps.points += 3
        
        elif m.player_one_score == m.player_two_score:
            for ps in player_standing:
                if ps.player.id == m.player_two:
                    ps.points += 1
                elif ps.player.id == m.player_one:
                    ps.points += 1

    return player_standing


def get_standings(league_id: int, order: str | None = None):
    matchups = get_tournament_matchups(league_id)
    players = player_service.get_tournament_players(league_id)
    
    player_standing = get_league_standing(players, matchups)
    
    return sorted(player_standing, key=lambda s: s.points, reverse = order != "asc")
#======================================

    
def update(data: Tournament, tournament: Tournament): 
    tournament.title = data.title
    if data.prize:
        tournament.prize = data.prize
        update_query("UPDATE tournament SET prize = ? where id = ?", (data.prize, tournament.id))
    if data.winner:
        tournament.winner = data.winner
        update_query("UPDATE tournament SET winner = ? where id = ?", (data.winner, tournament.id))

    update_query('''UPDATE tournament SET title = ? WHERE id = ? ''', (data.title, tournament.id))
    return tournament


def update_winner(tournament: Tournament, winner: Tournament): 
    update_query(
        '''UPDATE tournament SET winner = ? WHERE id = ? ''', (winner, tournament.id))


def force_update_matchup(id: int, new_matchup: MatchUp):
    if new_matchup.player_one_score:
        update_query('UPDATE matchups SET player_one_score = ? WHERE id = ?', (new_matchup.player_one_score, id))
    if new_matchup.player_two_score:
        update_query('UPDATE matchups SET player_two_score = ? WHERE id = ?', (new_matchup.player_two_score, id))
    if new_matchup.played_at:
        update_query('UPDATE matchups SET played_at = ? WHERE id = ?', (new_matchup.played_at, id))
    if new_matchup.player_one:
        update_query('UPDATE matchups SET player_one = ? WHERE id = ?', (new_matchup.player_one, id))
    if new_matchup.player_two:
        update_query('UPDATE matchups SET player_two = ? WHERE id = ?', (new_matchup.player_two, id))
        


#=========== Send added to tournament notification ===========
def added_to_tournament_mail(details: LinkedUserDetails, tournament: Tournament, starting_date: date):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{details.user_email}",
                "Name": f"{details.user_email}"
            }
            ],
            "Subject": "Added to tournament",
            "TextPart": f"You have been added to the {tournament.title} that is scheduled to start on {starting_date}. Good luck!",
            }
        ]
    }
    mailjet.send.create(data=data)


def send_email_to_linked_users(participants: list[str], tournament: Tournament, starting_date: date):
    linked_players = user_service.select_users_with_linked_players()

    for lp in linked_players:
        if f"{lp.player.first_name} {lp.player.second_name}" in participants:
            added_to_tournament_mail(lp, tournament, starting_date)


#========= Change participants ==========

def delete_matchups(tournament: TournamentResponseModel):
    update_query("DELETE from matchups where tournament_id = ?", (tournament.id,))


def get_starting_date(tournament: TournamentResponseModel):
    date = read_query("SELECT played_at from matchups where tournament_id = ? order by played_at asc limit 1", (tournament.id,))
    return date[0][0]


def create_new_matchups(tournament: TournamentResponseModel, participants: list[str], starting_date: str):
    if tournament.format.lower() == "league":
        participants = player_service.create_unknown_participants_profile(participants)
        days = get_phases(participants)

        for day in days:
            create_phase(tournament, participants, day, starting_date)
    
    elif tournament.format.lower() == "knockout":
        if len(participants) in [4,8,16,32,64,128,256]:

            participants = player_service.create_unknown_participants_profile(participants)       
            phase = 2
            p_count = len(participants)//2     
            
            while len(participants)>1:
                create_random_matchups(tournament, participants[0], participants[1], starting_date)
                participants.pop(0)
                participants.pop(0)       
            
            while p_count > 1:
                create_empty_phase(tournament, starting_date, phase, p_count)
                phase += 1
                p_count //= 2
        else:
            return Unauthorized('Participants should be 4, 8, 16, 32, 64, 128 or 256 count!')
    
    else: return BadRequest('Unknown format')

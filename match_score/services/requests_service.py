from datetime import datetime
from data import database
from data.models import LinkRequest, Player, PromotionRequest, User
from mailjet_rest import Client


pending_status = "pending"
denied_status = "denied"
accepted_status = "accepted"


#PROMOTION REQUESTS

def promotion_accepted_mail(user: User):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{user.email}",
                "Name": f"{user.email}"
            }
            ],
            "Subject": "Request Approved",
            "TextPart": "Congratulation! Your request for account promotion has been approved. You can now manage and create events on our platform. We are glad to have you on board!",
            }
        ]
    }
    mailjet.send.create(data=data)


def promotion_denied_mail(user: User):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{user.email}",
                "Name": f"{user.email}"
            }
            ],
            "Subject": "Request Denied",
            "TextPart": "Sadly your request for promotion has been denied. You can still access the normal feutures of our website.",
            }
        ]
    }
    mailjet.send.create(data=data)


def create_promotion_request(user: User):
    sent_at = datetime.utcnow()
    generated_id = database.insert_query(
        "INSERT into promotion_requests (user_id, sent_at, status) values (?,?,?)", 
        (user.id, sent_at, pending_status))

    request = PromotionRequest.from_query_result(generated_id, user.id, sent_at, pending_status, None)
    return request


def get_promotion_requests(status: str):
    if status == pending_status:
        data = database.read_query("SELECT * from promotion_requests where status = ?", (pending_status,))
    elif status == denied_status:
        data = database.read_query("SELECT * from promotion_requests where status = ?", (denied_status,))
    elif status == accepted_status:
        data = database.read_query("SELECT * from promotion_requests where status = ?", (accepted_status,))
    elif not status:
        data = database.read_query("SELECT * from promotion_requests")
    
    return (PromotionRequest(id=i[0], user_id=i[1], sent_at=i[2], status=i[3], admin_id=i[4]) for i in data)


def has_pending_promotion_request(user: User):
    req = database.read_query("SELECT * from promotion_requests where status = ? and user_id = ?", (pending_status, user.id))

    return req 


def promotion_request_by_id(id: int):
    data = database.read_query("SELECT * from promotion_requests where id = ?", (id,))
    if not data: return

    req = data[0]
    return PromotionRequest(id=req[0], user_id=req[1], sent_at=req[2], status=req[3], admin_id=req[4])


def approve_promotion_request(req_id: int, admin: User):
    admin_id = admin.id
    return database.update_query("UPDATE promotion_requests set status = ?, admin_id = ? where id = ?", (accepted_status, admin_id, req_id))


def deny_promotion_request(req_id: int, admin: User):
    admin_id = admin.id
    return database.update_query("UPDATE promotion_requests set status = ?, admin_id = ? where id = ?", (denied_status, admin_id, req_id))


#LINK PROFILE REQUESTS

def link_accepted_mail(user: User, player: Player):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{user.email}",
                "Name": f"{user.email}"
            }
            ],
            "Subject": "Request Approved",
            "TextPart": f"Your profile has been succesfully linked to {player.first_name} {player.second_name}. We are glad to have you on our site!",
            }
        ]
    }
    mailjet.send.create(data=data)


def link_denied_mail(user: User, player: Player):
    api_key = '9ebae64d6176dd19a3f36a58da793a96'
    api_secret = 'fcfc088b09bee693b6ff2988e9f5d1a9'
    mailjet = Client(auth=(api_key, api_secret), version='v3.1')
    data = {
        'Messages': [
        {
            "From": {
            "Email": "matchscore.team3@gmail.com",
            "Name": "Admin"
        },
            "To": [
            {
                "Email": f"{user.email}",
                "Name": f"{user.email}"
            }
            ],
            "Subject": "Request Denied",
            "TextPart": f"Your request to link your profile to {player.first_name} {player.second_name} has been rejected. You can still access the normal feutures of our website.",
            }
        ]
    }
    mailjet.send.create(data=data)


def create_link_request(user: User, player: Player):
    sent_at = datetime.utcnow()
    generated_id = database.insert_query(
        "INSERT into link_requests (user_id, player_id, sent_at, status) values (?,?,?,?)", 
        (user.id, player.id, sent_at, pending_status))

    request = LinkRequest.from_query_result(generated_id, user.id, player.id, sent_at, pending_status, None)
    return request


def get_link_requests(status: str):
    if status == pending_status:
        data = database.read_query("SELECT * from link_requests where status = ?", (pending_status,))
    elif status == denied_status:
        data = database.read_query("SELECT * from link_requests where status = ?", (denied_status,))
    elif status == accepted_status:
        data = database.read_query("SELECT * from link_requests where status = ?", (accepted_status,))
    elif not status:
        data = database.read_query("SELECT * from link_requests")
    
    return (LinkRequest(id=i[0], user_id=i[1], player_id=i[2], sent_at=i[3], status=i[4], admin_id=i[5]) for i in data)


def has_pending_link_request(user: User):
    req = database.read_query("SELECT * from link_requests where status = ? and user_id = ?", (pending_status, user.id))

    return req 


def link_request_by_id(id: int):
    data = database.read_query("SELECT * from link_requests where id = ?", (id,))
    if not data: return

    req = data[0]
    return LinkRequest(id=req[0], user_id=req[1], player_id=req[2], sent_at=req[3], status=req[4], admin_id=req[5])


def approve_link_request(req_id: int, admin: User):
    admin_id = admin.id
    return database.update_query("UPDATE link_requests set status = ?, admin_id = ? where id = ?", (accepted_status, admin_id, req_id))


def deny_link_request(req_id: int, admin: User):
    admin_id = admin.id
    return database.update_query("UPDATE link_requests set status = ?, admin_id = ? where id = ?", (denied_status, admin_id, req_id))


def null_request_player_id(player: Player):
    database.update_query("UPDATE link_requests set player_id = NULL where player_id = ?", (player.id,))
from datetime import date
import random
from fastapi import APIRouter, Header, Body
from data.models import Order, Participants, Scores, Tournament, MatchUp, Sort
from services import tournaments_service, user_service
from common.responses import NoContent, NotFound, BadRequest, Successful, Unauthorized

tournaments_router = APIRouter(prefix='/tournaments')

@tournaments_router.get('/')
def get_tournaments(sort: Sort | None = None, search: str | None = None):
    tournaments = tournaments_service.all(search)
    
    if not tournaments:
        return NotFound('No tournaments to view!')
    if sort:
        return tournaments_service.sort(tournaments, reverse=sort == 'desc')
    else:
        return tournaments


@tournaments_router.get('/{id}')
def get_tournament_by_id(id: int):
    tournament = tournaments_service.get_by_id(id)
    if not tournament: 
        return NotFound('Not such tournament')
    
    return tournament
        
    
@tournaments_router.post('/knockout')            
def create_knockout_tournament(tournament: Tournament, participants: list[str] = Body(), starting_date: date = Body(), x_token: str = Header()):  
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to create tournaments')
    if len(participants)==0:
        return Unauthorized('You cannot create an empty tournament!')
    
    random.shuffle(participants)
    
    if not tournament.format_id:
        return BadRequest("Tournament requires format")
    if not tournaments_service.check_for_existing_format(tournament.format_id):
        return BadRequest('Please enter a valid format!')
    if tournament.winner:
        return BadRequest('The tournament winner should be empty!')
    if starting_date < date.today():
        return BadRequest('Starting date should be today at the earliest!')
    if not tournament.format_id == 1:
        return ("You can not create leagues from here")   

    tournaments_service.send_email_to_linked_users(participants, tournament, starting_date)   
    tournaments_service.create_knockout_tournament(tournament, participants, starting_date)
    
    return Successful("Success")

@tournaments_router.put('/{id}')
def update_tournament(id: int, data: Tournament, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to update tournament')
    
    tournament = tournaments_service.get_by_id(id)
    if not tournament:
        return NotFound() 
    
    if data.winner and not tournaments_service.check_for_existing_player(data.winner):
        return Unauthorized('This player does not exist! Choose another winner!')
    
    updated_tournament = tournaments_service.update(data, tournament)
    return updated_tournament
        
    
@tournaments_router.put('/knockout/set_score/matchup/{id}')
def set_scores(id: int, scores: Scores, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to set scores!')
    
    if scores.score_one < 0 or scores.score_two < 0:
        return Unauthorized('Please enter a valid score!')
    
    if scores.score_one == scores.score_two:
        return Unauthorized('Please enter a valid score! Point a winner!')
    
    scores = [scores.score_one, scores.score_two]
    matchup = tournaments_service.get_matchup(id)
    if matchup.played_at > date.today():  
        BadRequest('This match hasnt been played yet! You cannot set score to it!')
    if not matchup: 
        return NoContent('Non existing matchup!')
    if not (matchup.player_one or matchup.player_two):
        return Unauthorized('There are no players in this match! You cannot set score yet!')
    if matchup.player_one_score or matchup.player_two_score:
        return BadRequest('There are scores already. You should update score in case of error.') 

    knockout = tournaments_service.get_by_id(matchup.tournament_id)
    if not knockout.format.lower() == "knockout":
        return ("You can not set league scores from here")  
        
    tournaments_service.set_matchup_score(id, scores)
    finnished_matchup = tournaments_service.get_matchup(id)
    next_matchup = tournaments_service.get_next_matchup(finnished_matchup)
    if next_matchup:    
        if next_matchup.player_one==finnished_matchup.player_one or next_matchup.player_one == finnished_matchup.player_two\
            or next_matchup.player_two == finnished_matchup.player_one or next_matchup.player_two == finnished_matchup.player_two:
            return Unauthorized()
        tournaments_service.update_participants_for_next_phase(next_matchup, finnished_matchup) 
    
    elif not next_matchup:
        tournament = tournaments_service.get_by_id(finnished_matchup.tournament_id)
        get_final_matchup_winner = finnished_matchup.player_one if finnished_matchup.player_one_score > finnished_matchup.player_two_score else finnished_matchup.player_two
        tournaments_service.update_winner(tournament, get_final_matchup_winner)
    
    return Successful("Success")

    

#========= Leagues ========

@tournaments_router.post('/league')
def create_league(league: Tournament, participants: list[str] = Body(), starting_date: date = Body(), x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to create leagues')
    
    if not league.format_id:
        return BadRequest("Tournament requires format")
    if len(participants) % 2 != 0:
        return BadRequest("A League needs even number of players")
    if starting_date < date.today():
        return BadRequest("Invalid date can no create tournaments in the past")
    if not tournaments_service.check_for_existing_format(league.format_id):
        return BadRequest('Please enter a valid format!')
    if league.winner:
        return BadRequest('The tournament winner should be empty!')
    if not league.format_id == 2:
        return ("You can not create knockout_tournaments from here") 
    
    tournaments_service.send_email_to_linked_users(participants, league, starting_date)
    tournaments_service.create_league(league, participants, starting_date)

    return Successful("Success")


@tournaments_router.put('/league/set_score/matchup/{id}')
def set_league_score(id: int, scores: Scores, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to set scores!')
    
    matchup = tournaments_service.get_matchup(id)
    if not matchup: 
        return NoContent('Non existing matchup!')
    if not (matchup.player_one or matchup.player_two):
        return Unauthorized('There are no players in this match! You cannot set score yet!')
    if matchup.player_one_score or matchup.player_two_score:
        return BadRequest('There are scores already. You should update score in case of error.')

    knockout = tournaments_service.get_by_id(matchup.tournament_id)
    if not knockout.format == "league":
        return ("You can not set knockout tournament scores from here") 
    
    scores = [scores.score_one, scores.score_two]
    tournaments_service.set_matchup_score(id, scores)

    return Successful("Success")


@tournaments_router.get('/league/standings/{league_id}')
def view_league_standings(league_id: int, order: Order | None = None):
    league = tournaments_service.get_by_id(league_id)
    if not league: 
        return NotFound("League does not exist")
    if not league.format == "league": 
        return BadRequest("This is not a league") 
    
    return tournaments_service.get_standings(league_id, order)



#========== Update Matchup(in case of mistake) and Update participants==========
@tournaments_router.put('/force/matchup/{id}')
def force_update_matchup(id: int, matchup: MatchUp, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to update a matchup')
    
    old_matchup = tournaments_service.get_matchup(id)
    if not old_matchup:
        return NotFound("Matchup does not exist")
    
    tournaments_service.force_update_matchup(id, matchup)

    return tournaments_service.get_matchup(id)


@tournaments_router.put('/force/participants/{id}')
def force_update_participants(id: int, participants: Participants, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth:
        return Unauthorized('Wrong or expired Token')
        
    user = user_service.find_user(user_auth)
    if not (user.is_admin() or user.is_director()):
        return Unauthorized('You are not authorized to update a matchup')
    
    tournament = tournaments_service.get_by_id(id)
    if not tournament:
        return NotFound('Tournament does not exist')
    
    starting_date = tournaments_service.get_starting_date(tournament)
    if starting_date < date.today():
        return BadRequest('Can update only future tournaments')
    
    tournaments_service.delete_matchups(tournament)
    tournaments_service.create_new_matchups(tournament, participants.participants, starting_date)

    return tournaments_service.get_by_id(id)

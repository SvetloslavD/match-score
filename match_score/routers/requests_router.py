from fastapi import APIRouter, Header
from services import requests_service, user_service, player_service
from data.models import Player, User, PromotionRequest, LogInfo, LinkRequest, Status
from common.responses import NotFound, Successful, Unauthorized, BadRequest



requests_router = APIRouter(prefix='/requests')


#Promotion endpoints
@requests_router.post('/promotion')
def make_promotion_request(x_token: str = Header()): 
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if user.is_admin() or user.is_director(): return BadRequest("You are already director")
    if requests_service.has_pending_promotion_request(user): return BadRequest("You already have a pending request")

    new_request = requests_service.create_promotion_request(user)
    if new_request: return Successful("You have sent a request")


@requests_router.get('/promotion')
def view_promotion_requests(status: Status | None = None, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin(): return Unauthorized("You do not have access to this information")

    return requests_service.get_promotion_requests(status)


@requests_router.put('/promotion/accept/{id}')
def accept_promotion_request(id: int, x_token: str = Header()):
    admin_auth = user_service.from_token(x_token)
    if not admin_auth: return Unauthorized("Wrong or expired token")

    admin = user_service.find_user(admin_auth)
    if not admin.is_admin(): return Unauthorized("Only admins can accept or deny requests")
    
    request = requests_service.promotion_request_by_id(id)
    if not request: return NotFound("The request does not exist")
    if not request.status == "pending": return BadRequest("Request is already approved or denied")

    request_sender = user_service.get_user_by_id(request.user_id)
    if not request_sender: return BadRequest("Unexpected Error")

    requests_service.approve_promotion_request(id, admin)
    requests_service.promotion_accepted_mail(request_sender)
    user_service.make_director(request_sender)

    return Successful("Request approved successfully")


@requests_router.put('/promotion/deny/{id}')
def deny_promotion_request(id: int, x_token: str = Header()):
    admin_auth = user_service.from_token(x_token)
    if not admin_auth: return Unauthorized("Wrong or expired token")

    admin = user_service.find_user(admin_auth)
    if not admin.is_admin(): return Unauthorized("Only admins can accept or deny requests")
    
    request = requests_service.promotion_request_by_id(id)
    if not request: return NotFound("The request does not exist")
    if not request.status == "pending": return BadRequest("Request is already approved or denied")

    request_sender = user_service.get_user_by_id(request.user_id)
    if not request_sender: return BadRequest("Unexpected Error")

    requests_service.deny_promotion_request(id, admin)
    requests_service.promotion_denied_mail(request_sender)

    return Successful("Request denied successfully")
    

#LinkProfile Endpoints

@requests_router.post('/linkprofile')
def make_link_profile_request(player: Player, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if user.is_admin() or user.is_director(): return BadRequest("You are already director")
    if user.associated_player: return BadRequest("You are already linked to a player profile")
    if requests_service.has_pending_link_request(user): return BadRequest("You already have a pending request")
    
    player = player_service.get_player_by_name_from_class(player)
    if not player: return NotFound("The player does not exist")
    if player_service.is_linked_to_user(player): return BadRequest("Player is already linked to a user")

    new_request = requests_service.create_link_request(user, player)
    if new_request: return Successful("You have sent a request")


@requests_router.get('/linkprofile')
def view_promotion_requests(status: Status | None = None, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin(): return Unauthorized("You do not have access to this information")

    return requests_service.get_link_requests(status)


@requests_router.put('/linkprofile/accept/{id}')
def accept_link_request(id: int, x_token: str = Header()):
    admin_auth = user_service.from_token(x_token)
    if not admin_auth: return Unauthorized("Wrong or expired token")

    admin = user_service.find_user(admin_auth)
    if not admin.is_admin(): return Unauthorized("Only admins can accept or deny requests")
    
    request = requests_service.link_request_by_id(id)
    if not request: return NotFound("The request does not exist")
    if not request.status == "pending": return BadRequest("Request is already approved or denied")

    request_sender = user_service.get_user_by_id(request.user_id)
    player = player_service.get_player_by_id(request.player_id)
    if not request_sender: return BadRequest("Unexpected Error")
    if not player: return NotFound("Player does not exist")

    requests_service.approve_link_request(id, admin)
    requests_service.link_accepted_mail(request_sender, player)
    user_service.link_profile(request_sender, player)

    return Successful("Request approved successfully")


@requests_router.put('/linkprofile/deny/{id}')
def deny_link_request(id: int, x_token: str = Header()):
    admin_auth = user_service.from_token(x_token)
    if not admin_auth: return Unauthorized("Wrong or expired token")

    admin = user_service.find_user(admin_auth)
    if not admin.is_admin(): return Unauthorized("Only admins can accept or deny requests")
    
    request = requests_service.link_request_by_id(id)
    if not request: return NotFound("The request does not exist")
    if not request.status == "pending": return BadRequest("Request is already approved or denied")

    request_sender = user_service.get_user_by_id(request.user_id)
    player = player_service.get_player_by_id(request.player_id)
    if not request_sender: return BadRequest("Unexpected Error")
    if not player: return NotFound("Player does not exist")

    requests_service.deny_link_request(id, admin)
    requests_service.link_denied_mail(request_sender, player)

    return Successful("Request denied successfully")
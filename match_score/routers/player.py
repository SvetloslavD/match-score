from fastapi import APIRouter, Header
from services import player_service, user_service, requests_service
from data.models import Player
from common.responses import BadRequest, NotFound, Successful, Unauthorized


player_router = APIRouter(prefix='/player')



@player_router.post('/')
def add_player(player: Player, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    if not user.is_admin() and not user.is_director(): return Unauthorized("Only directors or admins can create players")

    if not player.first_name: return BadRequest("First and second name are required")
    if not player.second_name: return BadRequest("First and second name are required")

    result = player_service.create_player(player)

    if not result: return BadRequest("Invalid team or country")
    return result


@player_router.get('/')
def all_players(country: str | None = None, team: str | None = None):
    return player_service.all_players(country, team)


@player_router.get('/{id}')
def get_player(id: int):
    player = player_service.get_player_by_id(id)

    if not player: return NotFound("Player does not exist")
    return player


@player_router.put('/{id}')
def update_team(id: int, player: Player, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)
    old_player = player_service.get_player_by_id(id)
    if not user.is_admin(): 
        if player_service.is_linked_to_user(old_player) and user.associated_player != old_player.id: 
            return Unauthorized("Only admins or the linked user can edit this player profile")
        elif not player_service.is_linked_to_user(old_player) and not user.is_director(): 
            return Unauthorized("Only directors can edit players")
    
    changed_player = player_service.update_player_team(id, player)

    if not changed_player: return BadRequest("Invalid team")
    return player_service.get_player_by_id(id)


@player_router.delete('/{id}')
def delete_player(id: int, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: return Unauthorized("Wrong or expired token")

    exists = player_service.get_player_by_id(id)
    if not exists: return NotFound("Player does not exist")

    user = user_service.find_user(user_auth)
    old_player = player_service.get_player_by_id(id)
    if not user.is_admin(): 
        if player_service.is_linked_to_user(old_player) and user.associated_player != old_player.id: 
            return Unauthorized("Only admins or the linked user can edit this player profile")
        elif not player_service.is_linked_to_user(old_player) and not user.is_director(): 
            return Unauthorized("Only directors can delete players")
    
    linked_user = player_service.is_linked_to_user(old_player)
    if linked_user:
        user_service.null_associated_player(linked_user)
        requests_service.null_request_player_id(old_player)

    player_service.delete_player(id)

    return Successful(f"Player {id} deleted")

@player_router.get('/statistics/{id}')
def get_player_statistics(id: int):
    player = player_service.get_player_by_id(id)
    if not player:
        return BadRequest('There is no such player!')
    return player_service.get_statistics(id)

from fastapi import APIRouter, Header,Body
from services import match_service, user_service, team_service
from data.models import PlayerMatchDetailUpdate, Match, TeamMatchDetailUpdate,TeamMatchRequest, MatchUpdate, Sort
from common.responses import BadRequest, NotFound, Unauthorized, Successful
from datetime import date


match_router = APIRouter(prefix='/match')


@match_router.get('/playerMatch/')                                  
def get_all_player_matches(sort: Sort | None = None):
    matches = match_service.all_player_matches()

    if sort:
        return match_service.sort(matches, reverse=sort == 'desc')
    else:
        return matches


@match_router.get('/teamMatch/')       
def get_all_team_matches(sort: Sort | None = None):
    matches = match_service.all_team_matches()

    if sort:
        return match_service.sort(matches, reverse=sort == 'desc')
    else:
        return matches


@match_router.get('/playerMatch/{id}')    
def get_match_by_id(id: int):
    match = match_service.get_with_players(id)
    if not match: 
        return NotFound('Not such match!')
    return match    


@match_router.get('/teamMatch/{id}')     
def get_match_by_id(id: int):
    match = match_service.get_with_teams(id)
    if not match: 
        return NotFound('No such match!')
    return match    


@match_router.post('/playerMatch')
def create_player_match(match: Match, participants: list[str] = Body(), x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: 
        return Unauthorized("Wrong or expired token")
    
    user = user_service.find_user(user_auth)    
    if not user.is_admin() and not user.is_director(): 
        return Unauthorized('You cannot create a match!')
    if match.played_at < date.today():
        return BadRequest("Match starting date cannot be in the past!")
    if not match.played_at:
        return BadRequest("Match starting time is required")
    if not match.title:
        return BadRequest("Match title is required")

    if len(participants) < 2:
        return BadRequest('You need 2 or more players to start a match!')
    match_service.create_match_with_players(match, participants)
    match_service.send_email_to_match_users(participants, match)
    
    return Successful('Match created successfully!')


@match_router.post('/teamMatch')
def create_team_match(match: Match, teams: list[str] = Body(), x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    if not user_auth: 
        return Unauthorized("Wrong or expired token")
    
    user = user_service.find_user(user_auth)    
    if not user.is_admin() and not user.is_director(): 
        return Unauthorized('You cannot create a match!')
    if match.played_at < date.today():
        return BadRequest("Match starting date cannot be in the past!")
    if not match.played_at:
        return BadRequest("Match starting time is required")
    if not match.title:
        return BadRequest("Match title is required")

    if len(teams) < 2:
        return BadRequest('You need 2 or more players to start a match!')
    
    for team in teams:
        if team not in team_service.team_names():
            return NotFound(f'No such team: {team}')
    
    match = match_service.create_with_teams(match, teams)
    
    return Successful('Match created successfully!')


@match_router.put('/{id}')                                                                                  #Updates title and played_at
def update_match(id: int, data: MatchUpdate, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    
    if not user_auth: 
        return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)    
    if not user.is_admin() and not user.is_director(): 
        return Unauthorized('You cannot update the match!')

    if data.played_at < date.today():
        return BadRequest("Match starting date cannot be in the past!")

    update = match_service.update(id, data)

    return update


@match_router.put('/playerMatchScore/{id}')                                                          #Updates player_id and score
def update_player_match_score(id: int, update: PlayerMatchDetailUpdate, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    
    if not user_auth: 
        return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth) 
       
    if not user.is_admin() and not user.is_director(): 
        return Unauthorized('You cannot update the match!')
    
    match = match_service.get_with_players(id)
    if not match: return NotFound('No such match!')

    update = match_service.update_player_match_score(id, update)

    return Successful("Score has been succesfully updated!")


@match_router.put('/teamMatchScore/{id}')                                                          #Updates team_id and score
def update_team_match_score(id: int, update: TeamMatchDetailUpdate, x_token: str = Header()):
    user_auth = user_service.from_token(x_token)
    
    if not user_auth: 
        return Unauthorized("Wrong or expired token")

    user = user_service.find_user(user_auth)    
    if not user.is_admin() and not user.is_director(): 
        return Unauthorized('You cannot update the match!')
    
    match = match_service.get_with_teams(id)
    if not match: return NotFound('No such match!')
    
    update = match_service.update_team_match_score(id, update)

    return Successful("Score has been succesfully updated!")







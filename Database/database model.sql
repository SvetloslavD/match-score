-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema match_score
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema match_score
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `match_score` DEFAULT CHARACTER SET latin1 ;
USE `match_score` ;

-- -----------------------------------------------------
-- Table `match_score`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`country` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 253
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`team` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`player`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`player` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `second_name` VARCHAR(45) NOT NULL,
  `team_id` INT(11) NULL DEFAULT NULL,
  `country_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_player_team1_idx` (`team_id` ASC) VISIBLE,
  INDEX `fk_player_country1_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_player_country1`
    FOREIGN KEY (`country_id`)
    REFERENCES `match_score`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_player_team1`
    FOREIGN KEY (`team_id`)
    REFERENCES `match_score`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 89
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) CHARACTER SET 'latin1' COLLATE 'latin1_bin' NOT NULL,
  `role` VARCHAR(45) NOT NULL,
  `associated_player` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_user_player1_idx` (`associated_player` ASC) VISIBLE,
  CONSTRAINT `fk_user_player1`
    FOREIGN KEY (`associated_player`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`link_requests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`link_requests` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `player_id` INT(11) NULL DEFAULT NULL,
  `sent_at` DATE NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `admin_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_link_requests_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_link_requests_player1_idx` (`player_id` ASC) VISIBLE,
  INDEX `fk_link_requests_user2_idx` (`admin_id` ASC) VISIBLE,
  CONSTRAINT `fk_link_requests_player1`
    FOREIGN KEY (`player_id`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_requests_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `match_score`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_requests_user2`
    FOREIGN KEY (`admin_id`)
    REFERENCES `match_score`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`match_format`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`match_format` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `rules` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`match`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`match` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `played_at` DATE NOT NULL,
  `match_format_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_match_match_format1_idx` (`match_format_id` ASC) VISIBLE,
  CONSTRAINT `fk_match_match_format1`
    FOREIGN KEY (`match_format_id`)
    REFERENCES `match_score`.`match_format` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`tournament_format`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`tournament_format` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `rules` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`tournament`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`tournament` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `prize` VARCHAR(45) NOT NULL,
  `tournament_format_id` INT(11) NOT NULL,
  `winner` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tournament_tournament_format1_idx` (`tournament_format_id` ASC) VISIBLE,
  INDEX `fk_tournament_player1_idx` (`winner` ASC) VISIBLE,
  CONSTRAINT `fk_tournament_player1`
    FOREIGN KEY (`winner`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournament_tournament_format1`
    FOREIGN KEY (`tournament_format_id`)
    REFERENCES `match_score`.`tournament_format` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`matchups`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`matchups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` INT(11) NOT NULL,
  `played_at` DATE NOT NULL,
  `tournament_phase` INT(11) NOT NULL,
  `player_one` INT(11) NULL DEFAULT NULL,
  `player_two` INT(11) NULL DEFAULT NULL,
  `player_one_score` INT(11) NULL DEFAULT NULL,
  `player_two_score` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_round_player1_idx` (`player_one` ASC) VISIBLE,
  INDEX `fk_round_player2_idx` (`player_two` ASC) VISIBLE,
  INDEX `fk_game_tournament1_idx` (`tournament_id` ASC) VISIBLE,
  CONSTRAINT `fk_game_tournament1`
    FOREIGN KEY (`tournament_id`)
    REFERENCES `match_score`.`tournament` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_round_player1`
    FOREIGN KEY (`player_one`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_round_player2`
    FOREIGN KEY (`player_two`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 305
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`player_match_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`player_match_detail` (
  `player_id` INT(11) NOT NULL,
  `match_id` INT(11) NOT NULL,
  `score` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`player_id`, `match_id`),
  INDEX `fk_match_detail_match1_idx` (`match_id` ASC) VISIBLE,
  CONSTRAINT `fk_match_detail_match1`
    FOREIGN KEY (`match_id`)
    REFERENCES `match_score`.`match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_detail_player`
    FOREIGN KEY (`player_id`)
    REFERENCES `match_score`.`player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`promotion_requests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`promotion_requests` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `sent_at` DATE NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `admin_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_promotion_requests_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_promotion_requests_user2_idx` (`admin_id` ASC) VISIBLE,
  CONSTRAINT `fk_promotion_requests_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `match_score`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_promotion_requests_user2`
    FOREIGN KEY (`admin_id`)
    REFERENCES `match_score`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `match_score`.`team_match_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `match_score`.`team_match_detail` (
  `match_id` INT(11) NOT NULL,
  `team_id` INT(11) NOT NULL,
  `score` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`match_id`, `team_id`),
  INDEX `fk_match_detail_match1_idx` (`match_id` ASC) VISIBLE,
  INDEX `fk_match_detail_copy1_team1_idx` (`team_id` ASC) VISIBLE,
  CONSTRAINT `fk_match_detail_copy1_team1`
    FOREIGN KEY (`team_id`)
    REFERENCES `match_score`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_match_detail_match10`
    FOREIGN KEY (`match_id`)
    REFERENCES `match_score`.`match` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;